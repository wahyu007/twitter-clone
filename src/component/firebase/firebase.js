import firebase from "firebase";

var firebaseConfig = {
  apiKey: "AIzaSyB1QOePQBA_EzJg1ZqtSRJL6LrOmgmedKI",
  authDomain: "twitter-clone-f47a2.firebaseapp.com",
  projectId: "twitter-clone-f47a2",
  storageBucket: "twitter-clone-f47a2.appspot.com",
  messagingSenderId: "559121150067",
  appId: "1:559121150067:web:c3870032e6806071f1fa06",
  measurementId: "G-NT69REZB2P",
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();

export default db;
