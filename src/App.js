import "./App.css";
import Feed from "./component/feed/Feed";
import Sidebar from "./component/sidebar/Sidebar";
import Widgets from "./component/widget/Widget";

function App() {
  return (
    <div className="app">
      <Sidebar />
      <Feed />
      <Widgets />
    </div>
  );
}

export default App;
